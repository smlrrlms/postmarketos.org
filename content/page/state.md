title: "State of postmarketOS"
---
postmarketOS is for Linux enthusiasts. For hackers, tinkerers, technical people
who are interested in pushing their mobile devices beyond what can be done with
the stock operating system: more free software, mainline kernel, getting
software updates until the hardware breaks, better privacy, less distracting
features, etc.

The goal is to make postmarketOS usable for non-technical people too, but we
are not there yet. Usability and most importantly stability issues need to be
worked out first. If you are looking for an OS that is as usable as iOS or
Android, this project is currently not for you. You will have the best
experience with postmarketOS after taking time to familiarize yourself with how
it works, making it your own and contributing to development and/or testing.
If you want to help us move forward and continue working on these issues, we
gladly accept [donations](https://opencollective.com/postmarketos).

What can be done with postmarketOS also depends on the
[device](https://wiki.postmarketos.org/wiki/Devices) you are running it on. If
you are just getting into Linux on Mobile, consider getting a second-hand
[SDM845](https://wiki.postmarketos.org/wiki/Qualcomm_Snapdragon_845_(SDM845))
or [MSM8916](https://wiki.postmarketos.org/wiki/Qualcomm_Snapdragon_410/412_(MSM8916))
device.

### Support requests

When compared to Android or iOS, postmarketOS is run by a tiny community of
developers who work on the project in their free time - typically next to a
dayjob, school or university. Keep that in mind when asking for help in the
issues or chat. This is not a product you paid for and you don't have a support
contract with us. People may help you if they have time, but sometimes nobody
can help you and you need to figure out problems on your own. Be nice to
everybody, follow the [Code of Conduct](https://postmarketos.org/coc).


### Testing

Long term we plan to improve stability by doing a lot more automated testing
(i.e. having phones in a test farm and continuously testing new software on
them). In the meantime, users like you can contribute to improving stability by
**[joining the testing team](https://wiki.postmarketos.org/wiki/Testing_Team).**

You will then get notifications when there is a brand new kernel or other
improvements to test out, and can tell us whether everything worked fine or if
you found a regression on your particular device.
