title: "Call For Tegra U-Boot Testers"
title-short: "Call For Tegra U-Boot Testers"
date: 2024-01-13
---
[#grid text#]

We currently have a generic port for Nvidia Tegra 2/3/4 devices using U-Boot
as bootloader, supporting 9 different devices at the time of writing. This
wouldn't have been possible without the work of Svyatoslav Ryhel (also known as
[Clamor](https://wiki.postmarketos.org/wiki/User:Clamor)), who has been working
on Tegra devices for the last few years and is also a maintainer of Nvidia
Tegra SoCs in U-Boot.

The
[wiki page](https://wiki.postmarketos.org/wiki/Nvidia_Tegra_armv7_(nvidia-tegra-armv7))
has a list of supported devices. 

Svyatoslav ported most of these devices without owning one, just relying on
testing from people. This means anyone having such a device can reach him
and eventually will be able to replace the old proprietary vendor bootloader
with U-Boot.

If you're interested, you can reach out Svyatoslav in
[#postmarketOS-on-tegra:matrix.org](https://matrix.to/#/#postmarketOS-on-tegra:matrix.org)
matrix room or e-mail him to get personal contacts at
[clamor95@gmail.com](mailto:clamor95@gmail.com).

[#grid end#]
