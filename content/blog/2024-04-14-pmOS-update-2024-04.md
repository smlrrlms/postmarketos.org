title: "postmarketOS in 2024-04: Grant applications and again, more TCs!"
title-short: "pmOS in 2024-04"
date: 2024-04-14
---
It is always fascinating to learn where postmarketOS ends up being used. This
month we learned how one does various cool things with their Sxmo powered
phone while travelling around the world on bike! Listen to the
[interview with magdesign](https://cast.postmarketos.org/episode/39-Interview-magdesign/)
in the latest podcast episode if you are curious. Now let's get to all the
other amazing things that happened since the last blog post.

## Pablo now works part-time on postmarketOS!

As it might be known to the reader, since December 2023
Clayton has been devoting [100% of his work time](https://freeradical.zone/@craftyguy/111506290148662542)
to postmarketOS. And since the beginning of this month Pablo is now also able to
invest [a big chunk of his work time](https://social.treehouse.systems/@pabloyoyoista/112196132250783169)
into doing postmarketOS development!

This is of course incredible for the project - more people working on
it means getting more things done faster. We still need to reach a sustainable
strategy for financing this work though. Right now it works by "setting
personal savings on fire" in combination to applying for grants, none of which
are approved yet (more on that below). Long term we would ideally be able to
cover their costs to work on postmarketOS from donations, but there is still
a long path ahead! Thanks to all the amazing people who are donating via our
[OpenCollective](https://opencollective.com/postmarketos/) we are already able
cover important costs, e.g: the
[post-FOSDEM hackathon](https://postmarketos.org/blog/2024/02/14/fosdem-and-hackathon/)
and all of our infrastructure costs, which is amazing!

On that note, we are also considering to free up more time for postmarketOS
development by paying a professional podcast editor instead of doing the editing
ourselves. (If you have feedback whether we should do this or not, let us know!)

## Grant applications

We have collaborated on sending out four grant applications in total, on which
various members of the wider Linux Mobile community with the right expertise
can work on, as well as members of the Core Team (as mentioned above). These
are the topics of the grants:

* Maintenance, CI, and testing, since these are in big need.
* VoLTE, so we can all enjoy phone calls, regardless of the country we live in.
* Building an optional, immutable/composable version of postmarketOS, that
  rules out a whole class of errors that can happen while upgrading
  postmarketOS on your phone. This is important, so postmarketOS is not only
  usable by hackers who know how to fix their system if an upgrade goes wrong.
* Upstreaming the hacks from
  [mobile-config-firefox](https://gitlab.com/postmarketOS/mobile-config-firefox/)
  to Firefox proper.

We hope to see some of these projects approved, and start to pay some of our
developers and community members so they can spend more time on things we need.
If you are a member of the community, with the will and ability (skills and
time) to work on relevant things in our ecosystem, feel free to contact
[Pablo](mailto:pabloyoyoista@postmarketos.org), he will be happy to help with
grant applications. Our current main missing priorities are related to HW
testing and phone cameras, but there are always other things that might be
possible and interesting to get funded for.

## So what's new?

* [Raymond Hackley](https://gitlab.com/wonderfulShrineMaidenOfParadise) and
  [Henrik Grimler](https://gitlab.com/grimler) became Trusted Contributors! Now
  there are more Trusted Contributors than people in the Core Team, which
  is great news. Welcome to the team and thanks to everybody who applied!

* We have 11 new device ports: PocketBook 614 Plus, Xiamomi Redmi 10C, Lenovo P2
  and K6 Note, Powkiddy X55, BQ Aquaris U Plus, Samsung Galaxy A2 Core, Ayn Odin,
  LG Stylo 3 Plus, ZTE ZXV10 B860HV5, and Asus Max M1. With the Chinese variants
  of Samsung Galaxy S5 now also being ported. It is remarkable, that some of
  those are very recent devices with the Xiaomi Redmi 10C hitting the market
  just two years ago! Thanks to all our device porters!

* Samsung Galaxy J7 Prime and Tab 3.8.0 and HTC One M8 devices gained mainline
  support, potentially extending the lifetime of those devices. Thanks
  [methanal](https://gitlab.com/methanal), [knuxify](https://gitlab.com/knuxify),
  and [Alexandre](https://gitlab.com/amessier)!

* Multiple kernels were updated to the latest stable release, 6.8. Thanks a lot
  to all our kernel maintainers!

* Since we allowed
  [official images for testing devices](https://wiki.postmarketos.org/wiki/Device_categorization#Official_Images)
  to be created, we now also build images for generic x86\_64, Fairphone 5,
  Microsoft Surface RT (before it moved was to community), and multiple
  Chromebooks! These are also available in the
  [download](https://postmarketos.org/download/) page. Thanks Clayton and
  Jenneron!

* There is a new UI:
  [Cage](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/4934)! It
  allows to boot postmarketOS in Kiosk mode with Wayland for use in
  single-application mode. Thanks [Vognev](https://gitlab.com/vognev)!

* Add extlinux configs to
  [msm8226](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/4939) and
  [msm8953](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/4999),
  which allows booting them with lk2nd (now that these SoCs are supported in
  lk2nd). Thanks [André](https://gitlab.com/a_a) and
  [Barnabás](https://gitlab.com/barni2000)!

* There is now generic device support for both
  [sdm845](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/4599) and
  [msm8916](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/4962)
  devices that benefit from using close-to-mainline U-Boot. Full integration
  is still work in progress, and the device transitions are not ready, but this
  is a huge step in the right direction. Thanks Caleb and Nikita!

* The Nokia N900 continues to get
  [improvements](https://gitlab.com/postmarketOS/pmaports/-/merge_requests?scope=all&state=merged&label_name[]=device-nokia-n900)
  and support, including integration of some modern tools like feedbackd. Thanks
  [Sicelo](https://gitlab.com/sicelo) for their continued work on this device!

* msm-firmware-loader [was upgraded to 1.5.0](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/4974),
  to provide more firmware from existing partitions. Thanks Alexey!

* There was a new mobile-config-firefox
  [4.3.0 release](https://gitlab.com/postmarketOS/mobile-config-firefox/-/tags/4.3.0)
  with lots of improvements, and a follow-up 4.3.1 release-fix.
  Thanks to [Peter](https://gitlab.com/1peter10) for all the contributions in
  these two releases, and for writing a very nice
  [blog post series](https://linmob.net/tags/mobile-config-firefox/)!

* The Microsoft Surface RT was
  [moved](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/4943) to
  community! Users can now expect better support for it. Thanks Jenneron!

* [unl0kr 3.1.0](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/5029)
  is now in postmarketOS edge. This release handles input device
  connection/disconnection at runtime among other things. Since 3.0.0, unl0kr
  is now being developed in the
  [BuffyBox](https://gitlab.com/postmarketOS/buffybox) repository, now part of
  a suite of graphical applications for the terminal. Thanks
  [Johannes](https://gitlab.com/cherrypicker)!

* More artwork and wallpapers are now
  [available](https://gitlab.com/postmarketOS/artwork/-/merge_requests/31),
  including a style guide for everybody that might want to contribute to it.
  We decided to set [Dikasp's](https://gitlab.com/dikasetyaprayogi) beautiful
  [meadow](https://gitlab.com/postmarketOS/artwork/-/blob/master/wallpapers/2024/meadow/contents/images/2707x2707.png?ref_type=heads)
  wallpaper as default for postmarketOS edge and the upcoming v24.06
  release, and that is now done for most UIs in edge ({{MR|4805|pmaports}},
  {{MR|5015|pmaports}}). Thanks Dikasp, Bart and Oliver!

* We released version
  [2.4.0](https://gitlab.com/postmarketOS/postmarketos-mkinitfs/-/tags/2.4.0) of
  mkinitfs, in preparation for compressed firmware support. Thanks Newbyte
  and Clayton!

* The GTK 4.14 release came with the new ngl renderer, which handles fractional
  scaling much better than the old gl renderer. We had already merged a release
  candidate of it earlier and wrote
  [an edge blog post](/edge/2024/02/05/gtk4-new-renderers/) about the possible
  breakage that comes with the new renderer. Nobody caught the regression with
  msm8916 and msm8953 in time, so as workaround the old renderer was
  re-enabled for these shortly after GTK 4.14 was merged ({{MR|4958|pmaports}},
  {{MR|4961|pmaports}}). Thanks [Nikita](https://gitlab.com/TravMurav) and
  [Andrea](https://gitlab.com/abologna)!

* The original PinePhone has now a
  [workaround](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/4937)
  to recover the modem when it crashes. Thanks
  [Arnav](https://gitlab.com/Arnavion)!

* gnss-share now
  [supports](https://gitlab.com/postmarketOS/gnss-share/-/merge_requests/15)
  providing location when there is no SIM installed. Thanks
  [Teemu](https://gitlab.com/tpikonen!)!

* msm8953 devices now use more upstream
  [firmware](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/4970)
  instead of carrying their own copies.
  Thanks [Barnabás](https://gitlab.com/barni2000)!

* Phosh was upgraded to 0.38.0
  [in Alpine edge](https://gitlab.alpinelinux.org/alpine/aports/-/merge_requests/63707).
  Thanks [fossdd](https://gitlab.alpinelinux.org/fossdd)!

* After [adding systemd to postmarketOS](/blog/2024/03/05/adding-systemd/) was
  announced, it was only possible to build your own systemd-based postmarketOS
  images if you check out a branch in pmbootstrap and build everything from
  scratch. The related pmbootstrap patches for bootstrapping systemd from plain
  Alpine ({{MR|2273|pmbootstrap}}) have been merged. Our package build server
  bpo has been adjusted to make use of it
  ({{issue|133|build.postmarketos.org}}), and a full staging repository has
  been built for x86\_64, aarch64 and armv7. Developers who want to hack on
  systemd in postmarketOS
  [can now use this temporary binary package repository](https://wiki.postmarketos.org/wiki/Systemd).
  While working on this, two bugs in "pmbootstrap build" were fixed
  ({{MR|2291|pmbootstrap}}, {{MR|2292|pmbootstrap}}). Thanks Oliver!

* pmbootstrap's bootimg\_analyze feature can now detect QCDT types
  ({{MR|2276|pmbootstrap}}) and it no longer logs a python trace on build
  failures ({{MR|2288|pmbootstrap}}). Thanks
  [Methanal](https://gitlab.com/methanal) and Newbyte!

## And what's next?

* A bleeding edge generic ARM64 EFI port is being worked on. It is called
  [trailblazer](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/4967)
  and will show how far device porting has come regarding pure upstream Linux.
  The goal is to give another incentive to just upstream everything - would it
  not be amazing if your particular device would work with the upstream Linux
  kernel, boot from upstream U-Boot, without any patching whatsoever?

* pmbootstrap and bpo will be further adjusted until it is possible to
  integrate systemd into pmaports.git master. The plan is to have the packages
  in an `extra-repos/systemd` directory. All packages from there will, as the
  name indicates, end up in a separate binary repository that will be used to
  extend the regular binary package repository if systemd is enabled
  ({{issue|2328|pmbootstrap}}). This has the advantage, that packages from this
  directory cannot be installed by accident on an OpenRC based installation.

* "pmbootstrap status" is getting a rewrite, to get rid the of more-or-less
  always failing checks it previously had, and to just have a minimal display
  of important information such as the currently checked out channel, branch,
  device, UI and whether systemd is enabled or not ({{MR|2294|pmbootstrap}}).

If you appreciate the work we're doing on postmarketOS, and want to support us,
consider [joining our OpenCollective](https://opencollective.com/postmarketos).
