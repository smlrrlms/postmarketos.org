title: "Adding systemd to postmarketOS"
title-short: "Adding systemd"
date: 2024-03-05
preview: "2024-03/systemd.jpg"
---

[![A OnePlus 6 running postmarketOS with tmux showing systemctl status and hostnamectl from systemd, surrounded by a GNOME and a KDE gear](/static/img/2024-03/systemd.jpg){: class="wfull border" }](/static/img/2024-03/systemd.jpg)

You heard it here first folks: systemd is coming to postmarketOS! As a mobile
oriented OS, our main goal has always been to work for everyone. From technical
folks to casual users. postmarketOS should have all the benefits you expect
from a Linux based distribution, such as being free software, respecting your
privacy, getting updates until your device physically breaks, respecting your
attention and not shoving advertisements in your face. Your phone should be a
tool you use, not the other way around.

This is of course not an easy task, one of the main blockers we found as we
collaborate more closely with KDE and GNOME developers is that they have a
hard time with our OpenRC-based stack. In
order to get KDE and GNOME working at all, we use a lot of systemd
[polyfills](https://en.wikipedia.org/wiki/Polyfill_(programming)) on top of
OpenRC. So while we are technically "not using systemd", in practice we already
do use a large chunk of its components to get KDE and GNOME running, just
different versions of those components. While we are very grateful for everybody
who works on these polyfills, we must point out that most aren't a full
replacement, and take additional effort to support and maintain. As much as we
might want to romanticise the idea of spending 6, 12, 24 months attempting to
come up with an even vaguely competitive alternative to systemd, we would quite
simply rather be working on making postmarketOS better.

So, after first seriously considering this idea almost a year ago, we have made
the decision to fix these incompatibilities and friction by just providing a
version of postmarketOS that builds on top of systemd. We are happy to announce
that it works, and we already have **[proof-of-concept images available that
you can try out right now!](https://mirror.postmarketos.org/temp.fdcc091b56f5/)**

The proof-of-concept images were made only for a few devices, as they had to be
built manually. You can also build your own by following the steps in the issue
below, or wait for us to get this integrated into the build system. The patches
that made the proof-of-concept possible currently live in branches of
pmaports.git and pmbootstrap.git, and will go through the usual review process
before we merge them. Read the following issue for an overview of building your
own images, what tasks still need to be done and where you could help out if
you are interested: **{{issue|2632|pmaports}}**

As with text editors, some people are really passionate about their favorite
init systems. When discussing this announcement, please keep a friendly tone.
Remember that we all share the love for free and open source software, and that
our communities work best if we focus on shared values instead of fighting over
what implementations to use.

This has been a pretty huge undertaking for us - as part of our effort to
sustainably grow postmarketOS into the mobile OS we all want it to be. If you
like what we do, please consider joining our
[OpenCollective](https://opencollective.com/postmarketOS).

# Q&A

## What about OpenRC, s6, runit, dinit?

For the die-hard fans of OpenRC and people who just have other use cases, such
as building a very minimal system that doesn't need all the polyfills anyway:
Don't worry, as long as OpenRC is in Alpine Linux (on which postmarketOS is
based), you will still be able to select it in
[pmbootstrap](https://wiki.postmarketos.org/wiki/Pmbootstrap)
when building your own images. Here is how it will look like, a new question in
<code>pmbootstrap init</code>:

[![pmbootstrap init asks whether the user wants to use systemd or not](/static/img/2024-03/pmbootstrap_init.png){: class="" }](/static/img/2024-03/pmbootstrap_init.png)

We have shown this announcement to [Sxmo](https://sxmo.org) maintainers in
advance, and they decided to stay with OpenRC. So pre-built images of Sxmo will
continue to be OpenRC based and we plan to make it so that pmbootstrap doesn't
ask about systemd after choosing Sxmo as UI.

Besides OpenRC, [s6](https://skarnet.org/software/s6/),
[runit](https://smarden.org/runit/) and
[dinit](https://davmac.org/projects/dinit/) are in Alpine. Right now we
don't have any UI using them in postmarketOS, but in theory they could be used
as well.

## Is Alpine cool with this?

Of course we also considered how this announcement would impact our (very
good!) relationship with Alpine Linux, which doesn't ship systemd. Even though
we are adding systemd, we certainly aren't planning to move away from Alpine.
We use Alpine for its incredibly fast package manager, the very readable and
easy to write APKBUILD package descriptions (that neatly allow splitting into
subpackages), the small/simple/secure musl libc, the awesome stable release
schedule, and just the huge selection of packages available in their repos. We
have shared this blog post with the Alpine devs before this publication, and we
hope that they understand our reasoning.

## What's wrong with the polyfills?

[Wikipedia](https://en.wikipedia.org/wiki/Polyfill_(programming)) describes the
term polyfill as "code that implements a feature of the development environment
that does not natively support the feature". Here is an overview of the ones
we have in our stack for compatibility with systemd, how maintained they are
and what they are missing.

[#grid side#]
[![A table of polyfills currently in use in postmarketOS.
maintained, implements all features: openrc-settingsd, eudev;
maintained, missing features: elogind, superd, logbookd;
unmaintained: waked, corecollector](/static/img/2024-03/polyfills.svg){: class="w400min" }](/static/img/2024-03/polyfills.svg)
[#grid text#]

* [`openrc-settingsd`](https://gitlab.com/postmarketOS/openrc-settingsd/)
  (`hostnamed`, `localed`, `timedated`): provides various D-Bus APIs that allow
  UIs to change locales, timezones etc. Originally, this was maintained by
  Gentoo's GNOME desktop team until we took over maintenance in 2021. Pablo
  wrote about it: "Luca and me have spent some precious time on fixing crashes
  and old legacy code on that, which we'd happily spend somewhere else. For a
  long time it would just crash upon trying to change language on first boot,
  and things like that."

* [`eudev`](https://github.com/eudev-project/eudev) (`udev`): userspace devfs
  daemon, forked from systemd code.

[#grid end#]

* [`elogind`](https://github.com/elogind/elogind) (`logind`): also forked from
  systemd code, implements the login1 D-Bus interface and systemd C APIs. Big
  chunks of the functionality are removed with
  [`#if 0`](https://github.com/elogind/elogind/blob/c36155c75f3c9288bcde42fa92ed2a8ade4031cd/src/libelogind/sd-journal/journal-send.c#L473)
  in the code, such as
  [`sd_journal_stream_fd`](https://www.freedesktop.org/software/systemd/man/latest/sd_journal_stream_fd.html).
  This breaks getting proper logs from `gnome-session`.

* [`superd`](https://sr.ht/~craftyguy/superd/) (`systemd --user`): runs various
  daemons in the user's session, such as
  [callaudiod](https://gitlab.com/mobian1/callaudiod) for accepting phone
  calls. It is crucial that if these daemons are crashing, they are properly
  restarted. superd was invented to be compatible with systemd user service
  files, and as of writing it is being used in Sxmo. But due to various
  incompatibilites it does not integrate well enough with `gnome-session` and
  cannot be used there. So right now we just don't supervise userspace daemons
  there and they don't restart if they crash. We can't use it with Plasma
  either, and if we had proper systemd user services instead, Plasma would have
  a faster session startup since parts of the session could be started in
  parallel and were synced (without race conditions).

* [`logbookd`](https://git.sr.ht/~martijnbraam/logbookd) (`journald`): receives
  kernel log messages and syslog messages and stores them in a sqlite database
  for querying. It works with daemons, but it does not implement the journalctl
  API and so one looses all session logs of a GNOME session.

* [`waked`](https://gitlab.com/seath1/waked) (`systemd.timer`): a
  proof-of-concept to let alarm clock apps wake the system from suspend at
  requested times. The project is
  [unmaintained](https://gitlab.com/seath1/waked/-/issues/3), and at least in
  GNOME clocks, `systemd.timer` will be used to implement this functionality on
  a maintained API. We don't have any other polyfill for `systemd.timer`.

* [`corecollector`](https://github.com/Cogitri/corecollector)
  (`systemd-coredumpd`) [^1]: Not needed to run KDE and GNOME, but a very
  useful tool that many developers expect to be able to use to easily retrieve
  coredumps and associated metadata, without first figuring out how to
  configure them properly. It is great that there is a polyfill for it, but it
  is unmaintained since 2020. Plasma Mobile developer Devin wrote:
  "When debugging crashes, it’s incredibly useful to be able to view
  coredumps and attach a debugger after the fact with coredumpctl, this
  can help us obtain backtraces that would otherwise be hard to
  replicate again with a debugger attached. We can also then enable
  DrKonqi’s coredumpd integration, which can allow users to get
  notifications about process crashes and see them from a GUI. DrKonqi
  also has optional integration with our Sentry instance, which we can
  enable on developer installs to automatically send crashes to a web
  dashboard for us."

## What about systemd and musl libc?

Alpine Linux is based on musl libc, whereas systemd officially only supports
glibc. Our current understanding having spoken to systemd developers is that we
should be able to find a path that brings us much closer to upstream, if not
entirely.

## What fancy new features will we get with systemd?

* Granular [privilege controls](https://www.freedesktop.org/software/systemd/man/latest/systemd.exec.html)
* Powerful service [dependency](https://trstringer.com/systemd-critical-chain/)
  and [security](https://www.ctrl.blog/entry/systemd-service-hardening.html)
  analysis features
* Tight [cgroups integration](https://www.freedesktop.org/software/systemd/man/latest/systemd.resource-control.html#) both static and dynamic
* Socket activation (so you can print from your phone without having CUPS running all the time!)
* Built in [boot-time analysis](https://www.thegoodpenguin.co.uk/blog/reducing-boot-time-with-systemd/)

## What does this mean for device maintainers?

For devices where we build KDE- and GNOME-based images, any custom services
will need a systemd version. We plan to adjust the existing device ports as
part of integrating systemd.

[^1]: [Introducing Corecollector, a coredump collector written in D - Cogitri's blog](https://cogitri.github.io/post/05-introducing-corecollector/)
