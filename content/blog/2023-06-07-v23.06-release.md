title: "v23.06: From the GNOME Mobile 2023 Hackfest"
title-short: "v23.06"
date: 2023-06-07
preview: "2023-06/v23.06.jpg"
---

[![A lot of phones on a table, most of them running postmarketOS (also one with a Debian logo), and lots of stickers, mostly for postmarketOS, and a beer coaster of the cultivation space](/static/img/2023-06/v23.06.jpg){: class="wfull border" }](/static/img/2023-06/v23.06.jpg)

After FOSDEM 2023 a few people found themselves hacking collaboratively on
postmarketOS in a local cafe in Brussels. It was so productive and fun that we
figured doing a proper hackathon this year would be great. While we were
planning, we realized that also a GNOME Mobile Hackfest would take place soon —
so we just did a postmarketOS hackathon right before GNOME Mobile's event. In
fact, this post and the release are brought to you right from the Hackfest!

v23.06, like previous releases, is geared mainly towards Linux
enthusiasts as explained in our article [state of postmarketOS](/state).

## Supported devices

This stable release is available for the following 31 devices:

* ASUS MeMo Pad 7
* Arrow DragonBoard 410c
* BQ Aquaris X5
* Fairphone 4
* Lenovo A6000
* Lenovo A6010
* Motorola Moto G4 Play
* Nokia N900
* ODROID HC2
* OnePlus 6
* OnePlus 6T
* PINE64 PineBook Pro
* PINE64 PinePhone
* PINE64 PinePhone Pro
* PINE64 RockPro64
* Purism Librem 5
* SHIFT6mq
* Samsung Galaxy A3 (2015)
* Samsung Galaxy A5 (2015)
* Samsung Galaxy E7
* Samsung Galaxy Grand Max
* Samsung Galaxy S III (GT-I9300 and SHW-M440S)
* Samsung Galaxy S4 Mini Value Edition
* Samsung Galaxy Tab 2 7.0"
* Samsung Galaxy Tab 2 10.1"
* Samsung Galaxy Tab A 8.0
* Samsung Galaxy Tab A 9.7
* Wileyfox Swift
* Xiaomi Mi Note 2
* Xiaomi Pocophone F1
* Xiaomi Redmi 2

The list is the same as in the previous v22.12 release (+ SP1), except that the PineTab
has been removed. The PineTab can still be used with postmarketOS edge. If you
are interested in getting it back to the stable release, the first step would
be you stepping up to maintain the port - let us know in the issues or chat.

## Highlights


* For the first time, this stable release includes GNOME Mobile! If you haven't
  followed the amazing progress that was made to bring GNOME to phones and
  tablets, see
  [Towards GNOME Shell on Mobile](https://blogs.gnome.org/shell-dev/2022/05/30/towards-gnome-shell-on-mobile/),
  [GNOME Shell on mobile: An update](https://blogs.gnome.org/shell-dev/2022/09/09/gnome-shell-on-mobile-an-update/)
  and [GNOME Mobile Show & Tell](https://conf.linuxappsummit.org/event/5/contributions/161/)
  ([video](https://www.youtube.com/live/watch?v=J7-3Qj_oVMM&t=14272)).
  
* [Lots of improvements to GNOME Software](https://blogs.gnome.org/pabloyoyoista/2023/03/05/gs-and-pmos-a-bumpy-road/)

* Translations (`lang`) is installed by default now, and the default locale is
  `en_US.UTF-8` instead of `C.UTF-8`

* USB tethering is now functional
  ([pma!3816](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3819))

* Minimum password length in installer images has been changed from 8 to 6

* PineBook Pro: backlight control and audio works by default

### User Interfaces

<!-- ordered alphabetically -->

* GNOME Shell on Mobile 44_git20230405 <span class="new">new</span>

* [Phosh 0.27](https://gitlab.gnome.org/World/Phosh/phosh/-/releases/v0.27.0)
  replaces version 0.26 from v22.12 SP2. The document viewer Evince has been
  added to the apps that get installed by default.

* [Plasma 5.27.5](https://plasma-mobile.org/2023/01/30/january-blog-post/)
  replaces version 5.26.5 from v22.12.

* [Sxmo 1.14.0](https://lists.sr.ht/~mil/sxmo-announce/%3CCRJLVDQWO5TS.3BLF8Y8EG3GGG%40yellow-orcess%3E)
  replaces version 1.12.0 from v22.12.

## Testing and known issues

A huge thanks to everyone at the newly formed
[Testing Team](https://wiki.postmarketos.org/wiki/Testing_Team), who helped
greatly in finding and getting a number of regressions and bugs fixed right
before finalizing the release!

Notable regressions:

* Sxmo on Pocophone F1: screen keeps waking up
  ([#2160](https://gitlab.com/postmarketOS/pmaports/-/issues/2160))
* A3/A5/A7/E5/E7/Grand Max: audio regressions, e.g. speaker is not set to mono
  ([#1788](https://gitlab.com/postmarketOS/pmaports/-/issues/1788))
* Nokia N900: does not boot from pre-built image (workaround: install with pmbootstrap)
  ([#2161](https://gitlab.com/postmarketOS/pmaports/-/issues/2161))

## How To Get It

### New installation

For new installs, see [download](/download) and make sure to read the wiki page
for [your device](https://wiki.postmarketos.org/wiki/Devices).

### Upgrade

For existing installations, see the
[upgrade to a newer postmarketOS release](https://wiki.postmarketos.org/wiki/Upgrade_to_a_newer_postmarketOS_release)
wiki article. Note that you still need to make sure to have a stable network
connection when performing the release upgrade (there were plans to make it
more resilient, but it wasn't possible to do it in time for the release).

Manual steps after upgrading to this release:

* If you have `gnome-software` installed, make sure to also install `gnome-software-plugin-apk`
* Librem 5: to make the camera work, remove megapixels and install millipixels
* Consider installing `lang` for translations
* Consider removing `postmarketos-hidden-desktop-entries`
  ([more information](https://postmarketos.org/edge/2023/05/22/removed-hidden-entries/))

## A community effort

A big thanks to everybody who contributed to postmarketOS, to Alpine or to any
of the numerous upstream components we use — without you this would not be
possible!
