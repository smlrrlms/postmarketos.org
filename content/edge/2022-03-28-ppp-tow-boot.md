title: "PinePhone Pro: flashing Tow-Boot to the SPI and new pmOS installation required"
date: 2022-03-28
---
We plan to merge
[pmb!2870](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/2870)
in two days, on Wednesday 2022-03-30. Once it is merged, the PinePhone Pro
will boot with UEFI in postmarketOS. The supported way to boot the PPP with
postmarketOS from then on is through [Tow-Boot](https://tow-boot.org), which
needs to be flashed to the SPI.

**Previous postmarketOS installations will not boot anymore** unless both
Tow-Boot has been flashed to the SPI, and a new installation with pmbootstrap
has been performed. Otherwise it will fail due to the boot partition not being
formatted with FAT32 (if only installing Tow-Boot without reinstalling) or
because extlinux.conf has been deleted (if only upgrading the existing install
without installing Tow-Boot).

Note that the PinePhone Pro is still in the testing category of devices, and
this is the edge channel. We don't make such radical, breaking changes on
stable and should be able to avoid them in edge once the device enters a higher
[device category](https://wiki.postmarketos.org/wiki/Device_categorization).

### Motivation
This deprecates the custom U-Boot build we used before with PPP-specific
patches. As of writing, we get the following advantages with Tow-Boot:

* Installing postmarketOS to the eMMC is now possible
* USB Mass Storage mode (similar to JumpDrive)
* [Standardizing the boot process](https://tuxphones.com/booting-arm-linux-the-standard-way/)


### Steps

Wait until [pmb!2870](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/2870)
is merged and related binary packages have been built
(see [bpo](https://build.postmarketos.org)). Then hold off with upgrading your
existing installation and follow the *Installing Tow-Boot on the SPI* and
*Installing postmarketOS* sections in the already updated
[PinePhone Pro](https://wiki.postmarketos.org/wiki/PINE64_PinePhone_Pro_(pine64-pinephonepro))
wiki page.
