title: "Phosh, GNOME Mobile: EoG replaced with Loupe"
date: 2023-08-24
---

New installations of Phosh and GNOME Mobile will have Loupe as image viewer
instead of Eye of GNOME.

Loupe is the new image viewer for GNOME. It also works better on phones than
Eye of GNOME and does not require any gschema overrides to fit the narrow
screens of phones. The overrides have been removed from edge.

Do the change manually on existing installs:

```
# apk del eog
# apk add loupe
```

Related:
[pma!4342](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/4342)
