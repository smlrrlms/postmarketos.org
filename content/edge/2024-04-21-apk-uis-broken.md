title: "apk plugin breakage in GNOME Software and Plasma Discover"
date: 2024-04-21
---

Both GNOME Software and Plasma Discover are currently broken in postmarketOS
edge, and unfortunately also in stable (v23.12). The reason is that the code
interacting with apk broke for both.

As workaround, use apk from the command-line (via terminal or
[SSH](https://wiki.postmarketos.org/wiki/SSH)).

Upgrade your system:
```
$ sudo apk upgrade -a
```

Search for a package:
```
$ apk search pkgname
```

Install a package:
```
$ sudo apk add pkgname
```

Related issue: {{issue|2739|pmaports}}

UPDATE: Plasma Discover uses libapk-qt, which has been adjusted in version
0.4.6. The upgrade has been merged to Alpine edge and will be available to edge
users once Alpine builders have caught up.

UPDATE: GNOME Software uses apk-polkit-rs, which has been adjusted in version
2.1.1-r1. The upgrade has been merged to Alpine edge and 3.19 will be available
to edge and stable users once Alpine builders have caught up.
