title: "PinePhone Pro audio is broken"
date: 2022-08-16
---

Unless running a specific version of Tow-Boot (not the latest release), audio
is currently broken on the PinePhone Pro. A workaround is in
[pma!3356](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3356).

The PPP was only added to v22.06 in the last service pack. v22.06 is also affected.

See [pma#1601](https://gitlab.com/postmarketOS/pmaports/-/issues/1601) for more
information.
