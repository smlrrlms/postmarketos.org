title: "Plasma Bigscreen has been disabled on Edge until KDE 6.1 releases"
date: 2024-03-04
---

Plasma Bigscreen has been disabled on Edge until KDE 6.1 releases. It remains
available on v23.12. This was done due to that the latest release of Plasma
Bigscreen isn't compatible with KDE 6.0.

If you have an existing installation of postmarketOS Edge with Plasma Bigscreen, 
be careful running `# apk upgrade -a` as it may result in the user interface
being removed.

For now, the only options for users wishing to continue using Plasma Bigscreen
until it has been added back are to patiently wait and be careful with upgrades
(not recommended as you may miss out on security updates) or switch to v23.12
(also not recommended as we don't support downgrading from Edge to stable
releases).

Related: {{MR|4883|pmaports}}

